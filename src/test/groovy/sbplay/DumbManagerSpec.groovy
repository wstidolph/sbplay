package sbplay

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import spock.lang.Specification
/**
 * Created by Wayne Stidolph on 1/23/2015.
 */


//@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes = Application.class)
@SpringApplicationConfiguration(classes=Application.class)
class DumbManagerSpec extends Specification {
    @Autowired
    DumbManagerService dms

    def "testing spock works"(){
        expect:
        dms.getTheVal() == 6
    }

    def "testing service calc"(){
        expect:
        dms.calcValue(5) == 5+dms.getTheVal()
    }
}
