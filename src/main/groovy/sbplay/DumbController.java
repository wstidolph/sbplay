package sbplay;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class DumbController {
    private DumbManagerService dumbManagerService;

    @Autowired
    public DumbController(DumbManagerService dumbManagerService) {
        this.dumbManagerService = dumbManagerService;
    }
    @RequestMapping(value = "/")
    public String index(Model model) {

        model.addAttribute("issues",
                "none");//dumbManager.findOpenIssues());
        model.addAttribute("welcome", "Wayne");
        return "home";
    }
}
