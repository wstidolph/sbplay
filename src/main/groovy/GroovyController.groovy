import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

/**
 * Created by Wayne Stidolph on 1/25/2015.
 */
@Controller
class GroovyController {
    @RequestMapping("/hello")
    @ResponseBody
    String home() {
        "Hello from Wayne"
    }
}
